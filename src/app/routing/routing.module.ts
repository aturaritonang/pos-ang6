import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '../pages/home/home.component';
import { CategoryComponent } from '../pages/master/category/category.component';
import { VariantComponent } from '../pages/master/variant/variant.component';
import { ProductComponent } from '../pages/master/product/product.component';
import { LoginComponent } from '../pages/login/login.component';
import { NotFoundComponent } from '../pages/other/not-found/not-found.component';
import { AutoGuard } from '../guards/auto-guard';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'category', component: CategoryComponent, canActivate: [AutoGuard] },
  { path: 'variant', component: VariantComponent, canActivate: [AutoGuard] },
  { path: 'product', component: ProductComponent, canActivate: [AutoGuard] },
  { path: 'login', component: LoginComponent },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class RoutingModule { }

export const routingComponent = [
  HomeComponent,
  CategoryComponent,
  VariantComponent,
  ProductComponent,
  LoginComponent,
  NotFoundComponent
];
