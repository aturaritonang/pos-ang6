import { Component, OnInit } from '@angular/core';
// import { LocalStorageService } from 'ngx-webstorage';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { User } from './model/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'P.O.S Ang6';
  logText = 'Login';
  loggedIn: Boolean = false;
  user: User;
  routes = [
    { linkName: 'Home', url: 'home' },
    {
      linkName: 'Master', children: [
        { linkName: 'Category', url: 'category' },
        { linkName: 'Variant', url: 'variant' },
        { linkName: 'Product', url: 'product' }
      ]
    },
    { linkName: 'P.O.S', url: 'pos' }
  ];

  constructor(
    // private localStorage: LocalStorageService,
    private route: ActivatedRoute,
    private router: Router) {
  }

  ngOnInit() {
    const uName = localStorage.getItem('userName');
    if (uName) {
      this.loggedIn = true;
      this.logText = 'Logout';
      this.user = {
        userName: uName,
        password: ''
      };
    } else {
      this.loggedIn = false;
      this.logText = 'Login';
      this.user = new User();
    }
  }

  logout() {
    console.log('Logging out...');
    this.loggedIn = false;
    localStorage.removeItem('userName');
    localStorage.removeItem('userToken');
    this.router.navigate(['login'], { relativeTo: this.route });
  }
}
