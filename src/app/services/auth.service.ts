import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Config } from '../base/config';
import { User } from '../model/user';

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  constructor(private http: HttpClient) { }

  postAuth(user: User) {
    return this.http.post(Config.Url + '/api/auth', user);
  }
}
