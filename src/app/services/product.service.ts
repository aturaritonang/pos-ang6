import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Config } from '../base/config';
import { Product } from '../model/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  constructor(private http: HttpClient) { }

  get() {
    return this.http.get(Config.Url + '/api/product', {
      headers: new HttpHeaders({ 'x-access-token': localStorage.getItem('userToken') })
    });
  }
}
