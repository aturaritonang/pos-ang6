import { Injectable } from '@angular/core';
import { Category } from '../model/category';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Config } from '../base/config';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  categories: Category[];
  category: Category;
  constructor(private http: HttpClient) { }

  get() {
    return this.http.get(Config.Url + '/api/category', {
      headers: new HttpHeaders({ 'x-access-token': localStorage.getItem('userToken') })
    });
  }
}
