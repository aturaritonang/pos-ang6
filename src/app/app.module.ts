import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { HomeComponent } from './pages/home/home.component';

import { AuthService } from './services/auth.service';
import { CategoryService } from './services/category.service';
import { VariantService } from './services/variant.service';
import { ProductService } from './services/product.service';

import { HttpClientModule } from '@angular/common/http';

import { RoutingModule, routingComponent } from '../app/routing/routing.module';

import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { MasterModule } from './pages/master/master.module';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    routingComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RoutingModule,
    HttpClientModule,
    BsDropdownModule.forRoot(),
    MasterModule
  ],
  providers: [
    AuthService,
    CategoryService,
    VariantService,
    ProductService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
