import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../../../services/category.service';
import { Category } from '../../../model/category';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  title = 'List of Category';
  categories: Category[] = [];
  constructor(private categoryService: CategoryService) { }

  ngOnInit() {
    this.get();
  }

  get() {
    this.categoryService.get()
      .subscribe((data: any) => {
        this.categories = data;
      }, (err) => {
        console.log(err);
      },
        () => console.log('Categories loaded!')
      );
  }
}
