import { Component, OnInit } from '@angular/core';
import { Variant } from '../../../model/variant';
import { VariantService } from '../../../services/variant.service';

@Component({
  selector: 'app-variant',
  templateUrl: './variant.component.html',
  styleUrls: ['./variant.component.css']
})
export class VariantComponent implements OnInit {
  title = 'List of Variant';
  variants: Variant[];
  constructor(private variantService: VariantService) { }

  ngOnInit() {
    this.get();
  }

  get() {
    this.variantService.get()
      .subscribe((data: Variant[]) => {
        this.variants = data;
      },
        (err) => console.log(err),
        () => console.log('Variants are loaded!'));
  }
}
