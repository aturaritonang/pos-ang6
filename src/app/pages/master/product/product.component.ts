import { Component, OnInit } from '@angular/core';
import { Product } from '../../../model/product';
import { ProductService } from '../../../services/product.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  title = 'List of Product';
  products: Product[];
  constructor(private productService: ProductService) { }

  ngOnInit() {
    this.get();
  }

  get() {
    this.productService.get()
      .subscribe((data: Product[]) => {
        this.products = data;
      }, (err) => console.log(err),
        () => {
          console.log(this.products);
          console.log('Products are loaded!');
        }
      );
  }
}
