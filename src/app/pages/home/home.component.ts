import { Component, OnInit } from '@angular/core';

// import { LocalStorageService } from 'ngx-webstorage';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  userName = '';
  constructor() { }

  ngOnInit() {
    this.userName = localStorage.getItem('userName');
  }

}
