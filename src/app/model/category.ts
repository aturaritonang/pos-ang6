export class Category {
    _id: String;
    initial: String;
    name: String;
    active: Boolean;
}
