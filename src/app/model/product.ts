export class Product {
    _id: String;
    variantId: String;
    initial: String;
    name: String;
    description: String;
    price: Number;
    active: Boolean;
}
