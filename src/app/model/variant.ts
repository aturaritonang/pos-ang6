export class Variant {
    _id: String;
    categoryId: String;
    initial: String;
    name: String;
    active: Boolean;
}
